
=====
Twitter Config
=====

Twitter Configuration for popularity index. Use alongside django-popularity-index package.

Quick start
-----------

1. Add "twitter_config" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'twitter_config',
    ]
