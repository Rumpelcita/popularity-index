# Twitter credentials
TWITTER_AUTH = {
    'CONSUMER_KEY' : 'your_key',
    'CONSUMER_SECRET' : 'your_secret',
    'ACCESS_TOKEN_KEY' : 'your_key',
    'ACCESS_TOKEN_SECRET' : 'your_secret'
    }

TWITTER_USER = 'verified'
TWITTER_LIMIT = 100
TWITTER_LIMIT_TEST = 10
