from django.test import TestCase
from .utils import SocialMediaFetcher
from .broker import JobHandler
from .models import Profile
from tweepy import User

import datetime

# Create your tests here.
class SocialMediaFetcherTestCase(TestCase):
    fetcher = SocialMediaFetcher()
    
    def test_api_connection(self):
        credentials = self.fetcher.api.verify_credentials()
        self.assertTrue(credentials)
        self.assertIsInstance(credentials, User)
    
    def test_api_fetch_friends(self):
        friends = self.fetcher.get_friends(is_test=True)
        self.assertTrue(friends)

class JobHandlerTestCase(TestCase):
    handler = JobHandler()

    def setUp(self):
        Profile.objects.create(twitter_id=1, name= "Delores", followers = 500, popularity = 10)
        Profile.objects.create(twitter_id=2, name= "Franklin", followers = 20, popularity = 1)

    def test_broker_fetch_profiles(self):
        profiles = self.handler.fetch_profiles(is_test=True)
        self.assertTrue(profiles)

class JobHandlerEmptyTestCase(TestCase):
    handler = JobHandler()

    def test_broker_fetch_empty_profiles(self):
        profiles = self.handler.fetch_profiles(is_test=True)
        self.assertTrue(profiles)
        for profile in profiles:
            self.assertTrue(profile)
