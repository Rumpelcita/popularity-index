from .utils import SocialMediaFetcher
from .models import Job, Profile
from operator import itemgetter
from math import floor
from django.db import Error
from django.shortcuts import render_to_response

class JobHandler(object):    
    fetcher = SocialMediaFetcher()
    
    def fetch_profiles(self, is_test=False):
        try:
            job = Job(type="fetch", status="Searching DB")
            job.save()

            profiles = Profile.objects.all()
        except (Error) as e:
            return render_to_response("pop_index.html", {"message": e.__cause__})
                
        if not profiles:
            try:
                job.status = "Fetching data from Twitter"
                job.save()
                job.status = "Parsing profiles"
                job.save()
                profiles = self.fetcher.get_friends(is_test)
                job.status = "Scoring profile"
                job.save()
                profiles = self.score_profiles(profiles)
                job.status = "Done"
                job.save()
                return profiles
            except (Error) as e:
                try:
                    job.status = "Done with errors"
                    job.save()
                except (Error) as e:
                    return render_to_response("pop_index.html", {"message": e.__cause__})
        else:
            try:
                job.status = "Done"
                job.save()
                return profiles
            except (Error) as e:
                return render_to_response("pop_index.html", {"message": e.__cause__})
    
    def score_profiles(self, profiles):
        max_followers = profiles[0]['followers']
        for profile in profiles:
            profile['popularity'] = self.rate_popularity(max_followers, profile['followers'])
            self.save_profile(profile)
        return profiles
    
    def rate_popularity(self, max_followers, current_followers):
        return floor(current_followers * (9/max_followers) + 1)
        
    def save_profile(self, profile):
        profile_created = Profile(**profile)
        profile_created.save() 
