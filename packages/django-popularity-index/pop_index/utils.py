from twitter_config import config
import tweepy
import json
from tweepy import OAuthHandler
from operator import itemgetter
from django.shortcuts import render_to_response

class SocialMediaFetcher():
    
    consumer_key = config.TWITTER_AUTH['CONSUMER_KEY']
    consumer_secret = config.TWITTER_AUTH['CONSUMER_SECRET']
    access_token_key = config.TWITTER_AUTH['ACCESS_TOKEN_KEY']
    access_token_secret = config.TWITTER_AUTH['ACCESS_TOKEN_SECRET']
    
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token_key, access_token_secret)
    
    try:
        api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True)
    except (tweepy.TweepError) as e:
            pass
    
    def get_friends(self, is_test=False):
        user = config.TWITTER_USER
        
        if is_test:
            limit = config.TWITTER_LIMIT_TEST
        else:
            limit = config.TWITTER_LIMIT
        
        profiles = []
        try:
            items = tweepy.Cursor(self.api.friends, screen_name=user).items(limit)
            for item in items:
                profile = self.parse_profile(item._json)
                profiles.append(profile)
            profiles = sorted(profiles, key=itemgetter('followers'), reverse=True)
            return profiles
        except (tweepy.TweepError) as e:
            pass
    
    def parse_profile(self, item):
        profile = {
            'twitter_id': item['id'],
            'name': item['name'],
            'followers': item['followers_count'],
            'description': item['description'],
            'avatar_url': item['profile_image_url'],
            'popularity': 0
            }
        return profile
