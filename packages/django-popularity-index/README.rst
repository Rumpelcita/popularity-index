
=====
Popularity Index
=====

Popularity Index fetches users from Social Media and ranks them based on a Popularity Index.

At the moment it fetches data from Twitter using the 'verified' user and Twitter configs are fetched from the django-twitter-config package. To add more social media providers overrite the get_friends function in utils and provide a django-social-media-name package with your social media configurations. 


Quick start
-----------

1. Add "pop_index" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'pop_index',
    ]

2. Include the polls URLconf in your project urls.py like this::

    url(r'', include('pop_index.urls')),

3. Run `python manage.py migrate pop_index` to create the popularity index models.

4. Start the development server and visit http://127.0.0.1:8000/ to fetch the popularity index.

5. Visit http://127.0.0.1:8000/admin to check on job status.
