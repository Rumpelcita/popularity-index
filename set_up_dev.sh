python3 -m venv popularity-index
source popularity-index/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
python popularity_index/manage.py migrate pop_index
