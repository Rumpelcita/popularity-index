from django.apps import AppConfig


class TwitterConfigConfig(AppConfig):
    name = 'twitter_config'
