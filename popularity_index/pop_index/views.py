from django.shortcuts import render, redirect
from .broker import JobHandler

# Create your views here.

def home(request):
    return render(request, 'pop_index/home.html', {})

def process(request):
    return redirect('/pop_index')
    
def pop_index(request):
    job_handler = JobHandler()
    profiles = job_handler.fetch_profiles()
    return render(request, 'pop_index/pop_index.html', {'profiles': profiles})
