from django.db import models

# Create your models here.
class Job(models.Model):
    id = models.AutoField(primary_key=True)
    type = models.CharField(max_length=25)
    status = models.CharField(max_length=25)
        
class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    twitter_id = models.IntegerField()
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=350)
    avatar_url = models.CharField(max_length=350)
    followers = models.IntegerField()
    popularity = models.IntegerField()
