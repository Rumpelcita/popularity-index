from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^pop_index/', views.pop_index, name='pop_index'),
]
