function do_heavy_lifting() {
    $.ajax({
        type: 'GET',
        url: '/pop_index',
        beforeSend: function() {
            $('#loading').show();
        },
        success: function(data) {
            window.location = "/pop_index";
        },
        cache: false
    });
}
